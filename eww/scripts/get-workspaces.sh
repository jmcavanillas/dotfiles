#!/bin/bash

spaces (){
	WORKSPACES_NUMBER=$(hyprctl workspaces -j | jq -c 'map(.id) | max')

    seq 1 $WORKSPACES_NUMBER | jq -s -c
}

spaces
socat -u UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock - | while read -r line; do
	spaces
done